//
//  FindViewController.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 18.10.2022.
//

import UIKit

class FindViewController: UIViewController {

    let mainView = FindView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        addAction()
        
    }

    
    func addAction() {
        let action = UIAction { action in
            
            guard let text = self.mainView.textFieldFind.text else { return }
            
            var modifiedText = ""
            
            for char in text {
                var newChar: Character = "a"
                if char == " " {
                    newChar = "%"
                } else {
                    newChar = char
                }
                
                modifiedText.append(newChar)
            }
            
            Task {
            guard let dataFind = try await NetworkService.shared.getFindRezult(string: modifiedText) else {
                return }
            
            let vc = RezultFindViewController()
            vc.dataFind = dataFind
            vc.searchText = text
            vc.modalPresentationStyle = .fullScreen
                DispatchQueue.main.async {
                    self.present(vc, animated: true)
                }
            }
        }
        
      
        
        mainView.buttonGo.addAction(action, for: .touchUpInside)
    }
    
}


