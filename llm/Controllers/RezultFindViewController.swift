//
//  RezultFindViewController.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 18.10.2022.
//

import UIKit

class RezultFindViewController: UIViewController {

    let mainView = RezultFindView()
    

    
    var dataFind: DataFind?
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        mainView.collectionView.dataSource = self
        
        
        addActions()
    }
  
    
    func addActions() {
        let action = UIAction { _ in
            self.dismiss(animated: true)
            
        }
        self.dismiss(animated: true)
        mainView.buttonBack.addAction(action, for: .touchUpInside)
    }


}

extension RezultFindViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0: return dataFind?.campaigns.count ?? 0
        case 1: return dataFind?.products.count ?? 0
        default: return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CampaignCell.reuseID,
                                                          for: indexPath) as! CampaignCell
            
            if let dataFind = dataFind {
                let campaign = dataFind.campaigns[indexPath.item]
                
                
                cell.imageView.image = UIImage(named: "TestImage")
                cell.cashbackLabel.text = campaign.cashback
                
                Task {
                    if let imageData = try await NetworkService.shared.downloadImage(by: campaign.imageUrl) {
                        if let image = UIImage(data: imageData) {
                            cell.imageView.image = image
                        }
                    }
                }
            }

            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.reuseID,
                                                          for: indexPath) as! ProductCell
            
            if let dataFind = dataFind {
                let product = dataFind.products[indexPath.item]
                cell.productImageView.image = UIImage(named: "TestImage")
                cell.companyImageView.image = UIImage(named: "TestImage")
                cell.cashbackLabel.text = product.cashback
                cell.priceLabel.text = product.price
                cell.titleLabel.text = product.name
                
                Task {
                    if let companyImageData = try await NetworkService.shared.downloadImage(by: product.campaignImageUrl) {
                        if let image = UIImage(data: companyImageData) {
                            cell.companyImageView.image = image
                        }
                    }
                }
                
                Task {
                    if !product.imageUrls.isEmpty {
                        if let productImageData = try await NetworkService.shared.downloadImage(by: product.imageUrls[0]) {
                            if let image = UIImage(data: productImageData) {
                                cell.productImageView.image = image
                            }
                        }
                    }
                   
                }

            }

            
            
            return cell
        default:
            return UICollectionViewCell(frame: CGRect())
        }
        
    }
}

