//
//  ViewController.swift
//  llm
//
//  Created by Serj Potapov on 14.10.2022.
//

import UIKit

class StartViewController: UIViewController {
    
    var phoneNumber : String?
    var rezultData:DataCode?
    
    let mainView = StartView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view = mainView
       
        addActions()
        
    }
    
    func addActions()
    {
        //MARK: - экшн на поделючение к апи и открытие второго экрана
        let action = UIAction { _ in
            Task{
                self.phoneNumber = self.mainView.textFieldNubmer.text
                guard self.phoneNumber != nil else{ return }
                
                self.rezultData = await NetworkService.shared.getCode(number: self.phoneNumber!)
                
                if self.rezultData?.successful == true {
                    let smsViewViewController = SmsViewController()
                    smsViewViewController.modalPresentationStyle = .fullScreen
                    smsViewViewController.phoneNumber = self.phoneNumber
                    self.present(smsViewViewController, animated: true)
                    
                }
                else
                {
                    let alertController = UIAlertController(title: "Error", message: "Номер телефона введен неверно", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default)
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true)
                     
                }
            }
            
        }
        mainView.buttonGo.addAction(action, for: .touchUpInside)
    }
    
    
}

