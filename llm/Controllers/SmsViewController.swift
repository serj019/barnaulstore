//
//  SmsViewController.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 17.10.2022.
//

import UIKit

class SmsViewController: UIViewController {
    
    let mainView = SmsView()
    
    var phoneNumber: String?
    
    var code: String?
  
    var rezultData:DataCode?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view = mainView
        
       addAction()
    }
    
    func addAction(){
        let action = UIAction{_ in 
            Task{
                self.code = self.mainView.textFieldCode.text
                guard self.code != nil else{ return }
                
                
                self.rezultData = await NetworkService.shared.useCode(number: self.phoneNumber!, code: self.code!)
                
                if self.rezultData?.successful == true {
                    let vc = FindViewController()
                    self.present(vc, animated: true)
                    print("DA")
                    
                }
                else
                {
                    let alertController = UIAlertController(title: "Error", message: "Код введен неверно", preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: "OK", style: .default)
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true)
                    
                }
            }
        }
        self.mainView.buttonEnter.addAction(action, for: .touchUpInside)
    }
}
