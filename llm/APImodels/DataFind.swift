//
//  DataFind.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import Foundation

struct DataFind: Decodable {
    var successful: Bool
    var errorMessage: String
    var errorMessageCode: String
    var campaigns: [Campaign]
    var products: [Product]
    var more: Bool
    var moreCampaigns: Bool
}

