//
//  Action.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import Foundation

struct Action: Decodable {
    var value: String
    var text: String
}
