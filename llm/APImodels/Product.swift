//
//  Product.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import Foundation

struct Product: Decodable, Identifiable {
    var id: Int
    var cashback: String
    var price: String
    var name: String
    var campaignName: String
    var campaignImageUrl: String
    var paymentTime: String
    var imageUrls: [String]
    var actions: [Action]
}

