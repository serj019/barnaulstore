//
//  Campaign.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import Foundation

struct Campaign: Decodable, Identifiable {
    var id: Int
    var name: String
    var cashback: String
    var imageUrl: String
    var paymentTime: String
    var actions: [Action]
}


