//
//  StartView.swift
//  llm
//
//  Created by Serj Potapov on 14.10.2022.
//

import UIKit

class StartView: UIView {
    
    //MARK: - блок кода с добавлением элементов экрана
    let labelEnter = UILabel()
    let labelInfo = UILabel()
    let labelTelephone = UILabel()
    
    let textFieldNubmer = UITextField()
    
    let buttonGo = UIButton(type: .system)
    
    let labelGuide = UILabel()
    
    let line = CALayer()
    
    init() {
        super.init(frame: CGRect())

        setView()
        setConstraints()
        backgroundColor = .white
    }
    
    
    
    
    
    
//MARK: - дизайн вью элементов
    func setView()
    {
        labelEnter.text = "Войти"
        labelEnter.font = UIFont(name:"HelveticaNeue-Bold", size: 27.0)
        labelEnter.heightAnchor.constraint(equalToConstant:27).isActive = true
        
        labelInfo.text = "Введите номер телефона, чтобы войти или зарегистрироваться"
        labelInfo.numberOfLines = 0
       // labelInfo.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        labelTelephone.text = "Номер телефона"
        labelTelephone.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        textFieldNubmer.placeholder = "9001112233"
        textFieldNubmer.keyboardType = .numberPad
        textFieldNubmer.heightAnchor.constraint(equalToConstant: 15).isActive = true
        textFieldNubmer.borderStyle = .none
        
        
        line.frame = CGRect(x: 0, y:textFieldNubmer.frame.height+17, width:UIScreen.main.bounds.width-40, height: 2)
        line.backgroundColor = UIColor(red: 13/255, green: 13/255, blue: 13/255, alpha: 1).cgColor
        textFieldNubmer.layer.addSublayer(line)
        
        buttonGo.setTitle("Далее", for: .normal)
        buttonGo.backgroundColor = .lightGray
       // buttonGo.heightAnchor.constraint(equalToConstant: 50).isActive = true
        buttonGo.layer.cornerRadius = 22
        buttonGo.titleLabel?.font = .systemFont(ofSize: 26)
        
       // buttonGo.widthAnchor.constraint(equalToConstant: 90).isActive = true
        
        labelGuide.text = "Нажимая кнопку 'Далее' вы, соглашаетесь с условиями Пользовательского соглашения и с обработкой вашей персональной информации на условиям Политики конфиденциальности"
        labelGuide.numberOfLines = 0
       // labelGuide.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
//MARK: - расположение на экране вью элементов
    func setConstraints()
    {
        
        let stackElements = UIStackView(views: [labelEnter,labelInfo,labelTelephone,textFieldNubmer,buttonGo,labelGuide], axis: .vertical, spacing: 40)
        
        addSubview(stackElements)
        
        stackElements.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackElements.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor , constant: 20),
            stackElements.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
            stackElements.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
