//
//  SmsView.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 17.10.2022.
//

import UIKit

class SmsView: UIView {

    let labelProof = UILabel()
    let labelSmsCode = UILabel()
    let textFieldCode = UITextField()
    let line = CALayer()
    let buttonRepeat = UIButton(type: .system)
    let buttonEnter = UIButton(type: .system)
    
    let phoneNumber:String = ""
    init() {
        super.init(frame: CGRect())
        self.backgroundColor = .white
        
        setView()
        setConstraint()
    }
    
    
    func setView()
    {
        labelProof.text = "Подтверждение телефона"
        labelProof.font = UIFont(name:"HelveticaNeue-Bold", size: 27.0)
        labelProof.numberOfLines = 0
        
        labelSmsCode.text = "введите код из SMS, отправленного на номер \(phoneNumber)"
        labelSmsCode.numberOfLines = 0
        textFieldCode.placeholder = "****"
        textFieldCode.isSecureTextEntry = true
        
        line.frame = CGRect(x: 0, y:textFieldCode.frame.height+17, width:UIScreen.main.bounds.width-40, height: 2)
        line.backgroundColor = UIColor(red: 13/255, green: 13/255, blue: 13/255, alpha: 1).cgColor
        textFieldCode.layer.addSublayer(line)
        
        buttonRepeat.setTitle("Отправить код еще раз", for: .normal)
        buttonRepeat.titleLabel?.font = .systemFont(ofSize: 19)
        buttonRepeat.setTitleColor(.darkGray, for: .normal)
        buttonRepeat.backgroundColor = .white
        
        buttonEnter.setTitle("Продолжить", for: .normal)
        buttonEnter.titleLabel?.font = .systemFont(ofSize: 26)
        buttonEnter.setTitleColor(.white, for: .normal)
        buttonEnter.backgroundColor = .blue
        
    }
    
    func setConstraint()
    {
        let stackElements = UIStackView(views: [labelProof,labelSmsCode,textFieldCode,buttonRepeat,buttonEnter], axis: .vertical, spacing: 40)
        stackElements.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stackElements)
        
        NSLayoutConstraint.activate([
            stackElements.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor , constant: 20),
            stackElements.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
            stackElements.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
