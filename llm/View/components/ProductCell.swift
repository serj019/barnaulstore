//
//  ProductCell.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    static let reuseID = "ProductCell"
    
    let productImageView = UIImageView()
    let companyImageView = UIImageView()
    let titleLabel = UILabel()
    let priceLabel = UILabel()
    let cashbackLabel = CashbackLabel()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect())
        backgroundColor = .white
        setView()
        setConstraints()
    }
    
    func setView() {
        setLabels()
        setImages()
        
    }
    
    func setLabels() {
        titleLabel.numberOfLines = 2
        titleLabel.text = "Как Ваши дизайнеры, скорее всего нческие паттерны на Swift - Strategy (Стр"
        
        priceLabel.text = "999.00 RUB"
        priceLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
        cashbackLabel.text = "до 62.30%"
    }
    
    func setImages() {
        productImageView.image = UIImage(named: "TestImage")
        productImageView.contentMode = .scaleAspectFill
        productImageView.clipsToBounds = true
        
        companyImageView.image = UIImage(named: "TestImage")
        companyImageView.contentMode = .scaleAspectFill
        companyImageView.clipsToBounds = true
        
        
    }
    
    func setConstraints() {
        let vStack = UIStackView(views: [titleLabel, companyImageView, priceLabel, cashbackLabel], axis: .vertical, spacing: 4)
        
        let stack = UIStackView(views: [productImageView, vStack], axis: .horizontal, spacing: 24)
        vStack.alignment = .leading
        
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            stack.leftAnchor.constraint(equalTo: leftAnchor, constant: 2),
            stack.centerXAnchor.constraint(equalTo: centerXAnchor),
            stack.centerYAnchor.constraint(equalTo: centerYAnchor)])
        
        NSLayoutConstraint.activate([
            companyImageView.heightAnchor.constraint(equalToConstant: 30),
            companyImageView.widthAnchor.constraint(equalToConstant: 160)
        ])
        
        NSLayoutConstraint.activate([
            productImageView.widthAnchor.constraint(equalToConstant: 90)])
        
        NSLayoutConstraint.activate([
            cashbackLabel.widthAnchor.constraint(equalToConstant: 100),
            cashbackLabel.heightAnchor.constraint(equalToConstant: 28),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
