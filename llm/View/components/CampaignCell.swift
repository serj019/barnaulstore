//
//  CampaignCell.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

import UIKit

class CampaignCell: UICollectionViewCell {
    
    static let reuseID = "CampaignCell"
    
    let cardView = UIView()
    let imageView = UIImageView()
    let cashbackLabel = CashbackLabel()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect())
        backgroundColor = .white
        setView()
    }
    
    
    func setView() {
        setCardView()
        setImageView()
        setConstraints()
    }
    
    func setCardView() {
        cardView.backgroundColor = .white
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowRadius = 4
        cardView.layer.shadowOpacity = 0.5
        cardView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cardView.layer.cornerRadius = 12
    }
    
    func setImageView() {
        imageView.layer.cornerRadius = 12
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
    }

    func setConstraints() {
        
        addSubview(cardView)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        
        cardView.addSubview(imageView)
        cardView.addSubview(cashbackLabel)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        cashbackLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: cardView.topAnchor),
            imageView.leftAnchor.constraint(equalTo: cardView.leftAnchor),
            imageView.centerXAnchor.constraint(equalTo: cardView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: cardView.centerYAnchor)])
            
        NSLayoutConstraint.activate([
            cashbackLabel.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -6),
            cashbackLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 6),
            cashbackLabel.widthAnchor.constraint(equalToConstant: 100),
            cashbackLabel.heightAnchor.constraint(equalToConstant: 28),
        ])
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            cardView.leftAnchor.constraint(equalTo: leftAnchor, constant: 6),
            cardView.centerXAnchor.constraint(equalTo: centerXAnchor),
            cardView.centerYAnchor.constraint(equalTo: centerYAnchor)])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

