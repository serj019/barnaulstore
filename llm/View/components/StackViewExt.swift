//
//  StackViewExt.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 15.10.2022.
//

import UIKit

extension UIStackView {
    
    convenience init(views: [UIView],axis: NSLayoutConstraint.Axis,spacing: CGFloat)
    {
        
        self.init(arrangedSubviews: views)
        self.spacing = spacing
        self.axis = axis
    }
    
}
