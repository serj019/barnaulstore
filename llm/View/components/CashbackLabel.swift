//
//  CashbackLabel.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 21.10.2022.
//

import UIKit

class CashbackLabel: UILabel {
    
    init() {
        super.init(frame: CGRect())
        self.backgroundColor = .systemPink
        self.layer.cornerRadius = 14
        self.textColor = .white
        self.textAlignment = .center
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
