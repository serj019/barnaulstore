//
//  CompositionalLayoutManager.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 20.10.2022.
//

enum SectionNumber: Int {
    case campaigns = 0
    case products = 1
}

import UIKit

class CompositionalLayoutManager {
    
    static let shared = CompositionalLayoutManager(); private init() { }
    
    func createLayout() -> UICollectionViewLayout {
        
        let layout = UICollectionViewCompositionalLayout { section, _ in
            
            switch section {
            case SectionNumber.campaigns.rawValue:
                return self.createCampaigns()
            case SectionNumber.products.rawValue:
                return self.createProducts()
            default: return self.createCampaigns()
            }
            
        }
        
        return layout
    }
    
    func createCampaigns() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.49),
                                               heightDimension: .fractionalWidth(0.49))
        
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalWidth(0.5))
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       repeatingSubitem: item,
                                                       count: 2)
        
        group.interItemSpacing = NSCollectionLayoutSpacing.fixed(8)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 0
        section.orthogonalScrollingBehavior = .continuous
        
        return section
    }
    
    
    func createProducts() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalHeight(0.2))
        
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                               heightDimension: .fractionalHeight(1))
        
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, repeatingSubitem: item, count: 5)
        
        group.interItemSpacing = NSCollectionLayoutSpacing.fixed(1)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 1
        section.orthogonalScrollingBehavior = .none
        
        return section
    }
    
}



