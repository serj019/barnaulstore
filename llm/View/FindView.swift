//
//  FindView.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 18.10.2022.
//

import UIKit

class FindView: UIView {
    
    let textFieldFind = UITextField()
    let buttonGo = UIButton()
    
    init() {
        super.init(frame: CGRect())
        self.backgroundColor = .white
        
        setView()
        setConstraint()
    }
    
    
    func setView()
    {
        buttonGo.backgroundColor = .blue
        buttonGo.setTitle("go", for: .normal)
        buttonGo.layer.cornerRadius = 12
        
        textFieldFind.placeholder = "Поиск"
        
        textFieldFind.backgroundColor = .lightGray
        textFieldFind.layer.cornerRadius = 12
    }
    
    func setConstraint()
    {
        buttonGo.heightAnchor.constraint(equalToConstant: 40).isActive = true
        buttonGo.widthAnchor.constraint(equalToConstant: 220).isActive = true

        textFieldFind.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textFieldFind.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        let stack = UIStackView(views: [textFieldFind,buttonGo], axis: .vertical, spacing: 40)
        stack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
        
//        stack.heightAnchor.constraint(equalToConstant: 150).isActive = true
//        stack.widthAnchor.constraint(equalToConstant: 150).isActive = true

        NSLayoutConstraint.activate([
            stack.centerXAnchor.constraint(equalTo: centerXAnchor),
            stack.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
