//
//  RezultFindView.swift
//  BarnaulStore
//
//  Created by Serj Potapov on 18.10.2022.
//

import UIKit

class RezultFindView: UIView {
   
    let collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: CompositionalLayoutManager.shared.createLayout())
    
    let buttonBack = UIButton(type: .system)
    
    init() {
        super.init(frame: CGRect())
        self.backgroundColor = .white
        
        setView()
        setConstraint()
    }
    
    
    func setView() {

        buttonBack.backgroundColor = .brown
        buttonBack.setTitle("<", for: .normal)
        buttonBack.layer.cornerRadius = 12
        
        collectionView.register(CampaignCell.self,
                                forCellWithReuseIdentifier: CampaignCell.reuseID)
        collectionView.register(ProductCell.self,
                                forCellWithReuseIdentifier: ProductCell.reuseID)
    }
    
    func setConstraint() {


        let stack = UIStackView(views: [buttonBack,collectionView], axis: .vertical, spacing: 40)
        stack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
      //  addSubview(buttonBack)
        
//        stack.heightAnchor.constraint(equalToConstant: 150).isActive = true
//        stack.widthAnchor.constraint(equalToConstant: 150).isActive = true
        buttonBack.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        NSLayoutConstraint.activate([
            stack.centerXAnchor.constraint(equalTo: centerXAnchor),
            stack.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            stack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            stack.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    var tableView = UITableView()

}

