//
//  NetWork .swift
//  BarnaulStore
//
//  Created by Serj Potapov on 17.10.2022.
//

import Foundation
class NetworkService {
    static let shared = NetworkService()
    private init() {}
    
  //https://utcoin.one/loyality/login_step1?phone=9236415158
    
    
    //MARK: - создание урла и подключание для первого экрана
    private func createURL(phoneNumber: String) -> URL? {
        let tunnel = "https://"
        let server = "utcoin.one"
        let method = "/loyality/login_step1"
        let params = "?phone=\(phoneNumber)"
        let string = tunnel + server + method + params
        let url = URL(string: string)
        
        return url
    }
    
 
    
    func getCode(number: String) async -> DataCode? {
        
        guard let url = self.createURL(phoneNumber: number) else { return nil }
        guard let response = try? await URLSession.shared.data(from: url) else { return nil }
       
        
        
        let data = response.0
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let rezultData = try? jsonDecoder.decode(DataCode.self, from: data) else { return nil }
        
        return rezultData
    }
    
    //MARK: - создание урла и подключание для второго экрана
    //https://utcoin.one/loyality/login_step2?phone=+79236415158&password=<code>
    private func createURLwithCode(phoneNumber: String, code:String) -> URL? {
        let tunnel = "https://"
        let server = "utcoin.one"
        let method = "/loyality/login_step2"
        let params = "?phone=+7\(phoneNumber)&password=\(code)"
        let string = tunnel + server + method + params
        let url = URL(string: string)
        
        return url
    }
    
 
    
    func useCode(number: String,code:String) async -> DataCode? {
        
        guard let url = self.createURLwithCode(phoneNumber: number, code: code) else { return nil }
        guard let response = try? await URLSession.shared.data(from: url) else { return nil }
       
        
        
        let data = response.0
        
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let rezultData = try? jsonDecoder.decode(DataCode.self, from: data) else { return nil }
        
        return rezultData
    }
    
    private func createFindURL(findStr: String) -> URL? {
            let tunnel = "https://"
            let server = "utcoin.one"
            let method = "/loyality/search"
            let params = "?search_string=\(findStr)"
            let string = tunnel + server + method + params
            let url = URL(string: string)
            
            return url
        }
        
    func getFindRezult(string: String) async throws -> DataFind? {
            guard let url = self.createFindURL(findStr: string) else {
                print("Invalid url")
                return nil }
            guard let response = try? await URLSession.shared.data(from: url) else {
                print("Bad Response")
                return nil }

            let data = response.0
            
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            guard let rezultData = try? jsonDecoder.decode(DataFind.self,
                                                           from: data) else {
                print("Not Converted data")
                return nil }
            
            return rezultData
        }
    
    
    func downloadImage(by url: String) async throws -> Data? {
        guard let url = URL(string: url) else { return nil }
        let session = URLSession.shared
        let data = try await session.data(from: url).0
        return data
    }
}
